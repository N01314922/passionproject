﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;


namespace PassionProject.Models
{
    public class BugCMSContext : DbContext
    {
        public BugCMSContext()
        {

        }

        public DbSet<BugReport> BugReports { get; set; }
        public DbSet<webproject> Webprojects { get; set; }
    }
}