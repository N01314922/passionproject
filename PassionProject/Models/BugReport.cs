﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace PassionProject.Models
{
    public class BugReport
    {
        [Key]
        public int BugReportid { get; set; }

        public string Where { get; set; }

        public string What { get; set; }

        public string When { get; set; }

        public string Due { get; set; }

        public string status { get; set; }   
        
        //one webproject

        public virtual webproject webproject { get; set; }
    }
}