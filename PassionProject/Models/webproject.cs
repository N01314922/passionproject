﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace PassionProject.Models
{
    public class webproject
    {
        [Key]
        public int Webprojectid { get; set; }

        public string ProjectName { get; set; }

        public string ProjectLink { get; set; }

        public String AuthorName { get; set; }

        //one project to many bugs
        public virtual ICollection<BugReport> Bugs { get; set; }
    }
}