﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data.Entity;
using PassionProject.Models;

namespace PassionProject.Controllers
{
    public class webprojectController : Controller
    {
        private BugCMSContext Db = new BugCMSContext();
        // GET: webproject
        public ActionResult create()
        {
            return View();
        }

        //Psot Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Webprojectid,ProjectName,ProjectLink,AuthorName")] webproject webpro)
        {
            if (ModelState.IsValid)
            {
                Db.Webprojects.Add(webpro);
                Db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(webpro);
        }

        public ActionResult List()
        {

            return View(Db.Webprojects.ToList());
        }

        //get edit

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            webproject webrepo = Db.Webprojects.Find(id);
            if (webrepo == null)
            {
                return HttpNotFound();
            }
            return View(webrepo);
        }

        // POST: edit

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Webprojectid,ProjectName,ProjectLink,AuthorName")] webproject webpro)
        {
            {
                if (ModelState.IsValid)
                {
                    Db.Entry(webpro).State = EntityState.Modified;
                    Db.SaveChanges();
                    return RedirectToAction("List");
                }
                return View(webpro);
            }
        }


        //Details get
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            webproject webpro = Db.Webprojects.Find(id);
            if (webpro == null)
            {
                return HttpNotFound();
            }
            return View(webpro);
        }

        //Delete get

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            webproject webpro = Db.Webprojects.Find(id);
            if (webpro == null)
            {
                return HttpNotFound();
            }
            return View(webpro);
        }

        //Delete post

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            webproject webpro = Db.Webprojects.Find(id);
            Db.Webprojects.Remove(webpro);
            Db.SaveChanges();
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}