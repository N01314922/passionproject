﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data.Entity;
using PassionProject.Models;

namespace PassionProject.Controllers
{
    public class BugReportController : Controller
    {
        private BugCMSContext Db = new BugCMSContext();
        // GET:create BugReport
        public ActionResult Create()
        {
            return View();
        }
        //Psot Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BugReportid,Where,What,When,Due,status")] BugReport bugrepo)
        {
            if (ModelState.IsValid)
            {
                Db.BugReports.Add(bugrepo);
                Db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(bugrepo);
        }

        public ActionResult List()
        {

            return View(Db.BugReports.ToList());
        }

        //get edit

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BugReport bugrepo = Db.BugReports.Find(id);
            if (bugrepo == null)
            {
                return HttpNotFound();
            }
            return View(bugrepo);
        }

        // POST: edit

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BugReportid,Where,What,When,Due,status")] BugReport bugrepo)
        {
            {
                if (ModelState.IsValid)
                {
                    Db.Entry(bugrepo).State = EntityState.Modified;
                    Db.SaveChanges();
                    return RedirectToAction("List");
                }
                return View(bugrepo);
            }
        }

        //Details get
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BugReport BugReport = Db.BugReports.Find(id);
            if (BugReport == null)
            {
                return HttpNotFound();
            }
            return View(BugReport);
        }


        //Delete get

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BugReport bugrepo = Db.BugReports.Find(id);
            if (bugrepo == null)
            {
                return HttpNotFound();
            }
            return View(bugrepo);
        }

        //Delete post

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BugReport bugrepo = Db.BugReports.Find(id);
            Db.BugReports.Remove(bugrepo);
            Db.SaveChanges();
            return RedirectToAction("List");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}